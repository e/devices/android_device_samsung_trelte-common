# Seccomp filters
BOARD_SECCOMP_POLICY := device/samsung/trelte-common/seccomp

# SELinux
BOARD_SEPOLICY_DIRS += device/samsung/trelte-common/sepolicy
